<?php
include_once('../lib/lib.php');
$data = $conn->getproduct();

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>소나이팜 관리자</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">
    <?php include_once '../lib/menu.php' ?>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Product</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        상품리스트
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <button type="button" class="btn btn-success" onclick="$('#addModal').modal('show')">상품추가</button>
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>상품이름</th>
                                <th>수확날짜(월)</th>
                                <th>이미지(썸네일)</th>
                                <th>이미지(상세)</th>
                                <th>상세보기(링크)</th>
                                <th>상품구매(링크)</th>
                                <th>기능</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($data as $dd) {?>
                                <tr>
                                    <td><?=$dd['uid']?></td>
                                    <td><?=$dd['name']?></td>
                                    <td><?=$dd['grow']?>월</td>
                                    <td><img src="<?=$dd['image']?>" style="width: 100px; height: auto"/></td>
                                    <td><img src="<?=$dd['image_p']?>" style="width: 100px; height: auto"/></td>
                                    <td><a href="<?=$dd['more_url']?>">이동하기</a></td>
                                    <td><a href="<?=$dd['shop_url']?>">이동하기</a></td>
                                    <td>
                                        <button type="button" data-id="<?=$dd['uid']?>" class="btn btn-primary product_edit">수정</button>
                                        <button type="button" data-id="<?=$dd['uid']?>" class="btn btn-danger product_delete">삭제</button>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->
<!--                        <div class="well">-->
<!--                            <h4>DataTables Usage Information</h4>-->
<!--                            <p>DataTables is a very flexible, advanced tables plugin for jQuery. In SB Admin, we are using a specialized version of DataTables built for Bootstrap 3. We have also customized the table headings to use Font Awesome icons in place of images. For complete documentation on DataTables, visit their website at <a target="_blank" href="https://datatables.net/">https://datatables.net/</a>.</p>-->
<!--                            <a class="btn btn-default btn-lg btn-block" target="_blank" href="https://datatables.net/">View DataTables Documentation</a>-->
<!--                        </div>-->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title" id="exampleModalLongTitle">상품추가</h5>
            </div>
            <form action="../lib/product_handler.php" method="post" enctype="multipart/form-data" onsubmit="if(confirm('추가하시겠습니까?')){return true;}else{return false;}">
                <div class="modal-body">
                    <div class="form-group" style="display:none">
                        <label for="recipient-name" class="col-form-label">Hidden Value</label>
                        <input type="hidden" class="form-control" name="type" value="add">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">이름</label>
                        <input type="text" class="form-control" name="name" placeholder="이름을 입력해주세요." required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">수확날짜(월)</label>
                        <input type="number" min="1" max="12" class="form-control" placeholder="숫자만 입력해주세요(1~12)" name="grow" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">이미지(썸네일)</label>
                        <input type="file" class="form-control" placeholder="이미지업로드해주세요." name="image" accept="image/*" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">이미지(상세)</label>
                        <input type="file" class="form-control" placeholder="이미지업로드해주세요." name="image_p" accept="image/*" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">상품구매URL</label>
                        <input type="text" class="form-control" name="shop_url" placeholder="선택(기본값:#)">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">상세정보</label>
                        <textarea style="min-height: 300px; resize: vertical" class="form-control" name="info" placeholder="상세정보를 입력해주세요." required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">취소</button>
                    <button type="submit" class="btn btn-primary">추가</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title" id="exampleModalLongTitle">상품수정</h5>
            </div>
            <form action="../lib/product_handler.php" method="post" enctype="multipart/form-data"  onsubmit="if(confirm('수정하시겠습니까?')){return true;}else{return false;}">
                <div class="modal-body edit-modal-body">
                    <div class="form-group" style="display:none">
                        <label for="recipient-name" class="col-form-label">Hidden Value</label>
                        <input type="hidden" class="form-control" name="type" value="add">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">이름</label>
                        <input type="text" class="form-control" name="name" placeholder="이름을 입력해주세요." required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">수확날짜(월)</label>
                        <input type="number" min="1" max="12" class="form-control" placeholder="숫자만 입력해주세요(1~12)" name="grow" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">이미지(썸네일)</label>
                        <input type="file" class="form-control" placeholder="이미지업로드해주세요." name="image" accept="image/*" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">이미지(상세)</label>
                        <input type="file" class="form-control" placeholder="이미지업로드해주세요." name="image_p" accept="image/*" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">상품구매URL</label>
                        <input type="text" class="form-control" name="shop_url" placeholder="선택(기본값:#)">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">상세정보</label>
                        <textarea style="min-height: 300px; resize: vertical" class="form-control" name="info" placeholder="상세정보를 입력해주세요." required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">취소</button>
                    <button type="submit" class="btn btn-primary">수정</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- jQuery -->
<script src="../vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../vendor/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });

    $('.product_delete').click(function(){
        var id = $(this).attr('data-id');
        if(confirm('삭제하시겠습니까?\n삭제하시면 복구가 불가능합니다.')){
            $.ajax({
                url: '../lib/product_handler.php',
                type: 'post',
                data : {
                    type : 'delete',
                    id: id
                },success : function(){
                    alert('삭제되었습니다.');
                    location.reload();
                },error : function(e){
                    console.log(e);
                    alert('삭제를 실패했습니다. 확인후 다시 시도해주세요.')

                }
            })
        }else{
            return false;
        }
    });

    $('.product_edit').click(function(){
        var id = $(this).attr('data-id');
        $.ajax({
            url: '../lib/product_handler.php',
            type: 'post',
            data : {
                type : 'get',
                id: id
            },success : function(data){
                $('.edit-modal-body').empty().append(data);
                $('#editmodal').modal('show');
            },error : function(e){
                console.log(e);
                alert('수정데이터를 가져오기 실패했습니다.')

            }
        })
    })

</script>

</body>

</html>
