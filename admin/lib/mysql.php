<?php
include_once 'config.php';

class mysql {
    private $conn;
    private $host = DB_HOST;
    private $database = DB_NAME;
    private $username = DB_USER;
    private $password = DB_PASS;
    private $charset = DB_CHAR;
    private $error_message;
    private $contents1 = Array();
    private $contents2 = Array();
    private $contents3 = Array();

    public function __construct()
    {
        $dsn = 'mysql:host=' . $this->host . ';port=3306;dbname=' . $this->database . ';charset=' . $this->charset;

        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );

        try {
            $this->conn = new PDO($dsn, $this->username, $this->password, $options);
        } catch (PDOException $e) {
            $this->error_message = $e->getMessage();
        }
    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
    }

    public function intl_output_error(){
        return $this->error_message;
    }


    public function test(){
        try{
            $stmt = $this->conn->prepare("select * from  test limit 0,2");
            $stmt->execute();
            $this->contents1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $this->contents1;
        }catch (PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }

    //상품리스트 가져오기
    public function getproduct() {
        try{
            $stmt = $this->conn->prepare("select * from  product where is_delete != 1");
            $stmt->execute();
            $this->contents1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $this->contents1;
        }catch (PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }
    //상품리스트 가져오기
    public function getproduct_byid($id) {
        try{
            $stmt = $this->conn->prepare("select * from  product where uid = :id");
            $stmt->bindParam('id',$id,PDO::PARAM_STR);
            $stmt->execute();
            $this->contents1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $this->contents1[0];
        }catch (PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }

    public function deleteproduct($id){
        try{
            $stmt = $this->conn->prepare("UPDATE product set is_delete = 1 where uid = :productid");
            $stmt->bindParam('productid',$id,PDO::PARAM_STR);
            $stmt->execute();
            return true;
        }catch (PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }

    public function addproduct($name,$grow,$image,$image_p,$more_url='#',$shop_url='#',$info,$remarks='-'){
        try{
            if(empty($more_url)){
                $more_url = '#';
            }
            if(empty($shop_url)){
                $shop_url = '#';
            }
            if(empty($remarks)){
                $remarks = '-';
            }
            $stmt = $this->conn->prepare('INSERT INTO `product`(`uid`, `name`, `grow`, `image`, `image_p`, `more_url`, `shop_url`, `info`,  `remarks`) VALUES (DEFAULT ,:name,:grow,:image,:image_p,:more_url,:shop_url,:info,:remarks)');
            $stmt->bindParam('name',$name,PDO::PARAM_STR);
            $stmt->bindParam('grow',$grow,PDO::PARAM_STR);
            $stmt->bindParam('image',$image,PDO::PARAM_STR);
            $stmt->bindParam('image_p',$image_p,PDO::PARAM_STR);
            $stmt->bindParam('more_url',$more_url,PDO::PARAM_STR);
            $stmt->bindParam('shop_url',$shop_url,PDO::PARAM_STR);
            $stmt->bindParam('info',$info,PDO::PARAM_STR);
            $stmt->bindParam('remarks',$remarks,PDO::PARAM_STR);
            $stmt->execute();
            return true;
        }catch (PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }

    public function updateproduct($id,$name,$grow,$image,$image_p,$more_url='#',$shop_url='#',$info,$remarks='-'){
        try{
            if(empty($more_url)){
                $more_url = '#';
            }
            if(empty($shop_url)){
                $shop_url = '#';
            }
            if(empty($remarks)){
                $remarks = '-';
            }
            $stmt = $this->conn->prepare('UPDATE product set name = :name, grow = :grow, image = :image, image_p = :image_p, more_url = :more_url, shop_url = :shop_url, info = :info, remarks = :remarks where uid = :id');
            $stmt->bindParam('id',$id,PDO::PARAM_STR);
            $stmt->bindParam('name',$name,PDO::PARAM_STR);
            $stmt->bindParam('grow',$grow,PDO::PARAM_STR);
            $stmt->bindParam('image',$image,PDO::PARAM_STR);
            $stmt->bindParam('image_p',$image_p,PDO::PARAM_STR);
            $stmt->bindParam('more_url',$more_url,PDO::PARAM_STR);
            $stmt->bindParam('shop_url',$shop_url,PDO::PARAM_STR);
            $stmt->bindParam('info',$info,PDO::PARAM_STR);
            $stmt->bindParam('remarks',$remarks,PDO::PARAM_STR);
            $stmt->execute();
            return true;
        }catch (PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }

    //패스워드 암호화
    public function password_encrypt($password){
        return openssl_encrypt($password,"AES-256-CBC", hash('sha256',ENC_KEY),0,hash('sha256',ENC_VIK));
    }

//패스워드 복호화
    protected function password_decrypt($password){
        return openssl_decrypt($password,"AES-256-CBC", hash('sha256',ENC_KEY),0,hash('sha256',ENC_VIK));
    }

    public function login($id,$pw){
        try{
            $stmt = $this->conn->prepare('SELECT uid from users where email = :id and password = :pw');
            $stmt->bindParam('id',$id,PDO::PARAM_STR);
            $stmt->bindParam('pw',$this->password_encrypt($pw),PDO::PARAM_STR);
            $stmt->execute();
            $this->contents1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $this->contents1[0]['uid'];
        }catch (PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }

    public function getuser($id){
        try{
            $stmt = $this->conn->prepare('SELECT * from users where uid = :id');
            $stmt->bindParam('id',$id,PDO::PARAM_STR);
            $stmt->execute();
            $this->contents1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $this->contents1[0];
        }catch (PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }

    public function editeuser($id,$email,$nick,$pw = null){
        try{
            if(!empty($pw)){
                $stmt = $this->conn->prepare('UPDATE users set email = :email, password = :passwd, nickname = :nick where uid = :id');
                $stmt->bindParam('passwd',$this->password_encrypt($pw),PDO::PARAM_STR);
            }else{
                $stmt = $this->conn->prepare('UPDATE users set email = :email, nickname = :nick where uid = :id');
            }

            $stmt->bindParam('id',$id,PDO::PARAM_STR);
            $stmt->bindParam('email',$email,PDO::PARAM_STR);
            $stmt->bindParam('nick',$nick,PDO::PARAM_STR);
            $stmt->execute();
            return true;
        }catch (PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }

}