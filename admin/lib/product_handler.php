<?php
header('Content-Type: text/html; charset=UTF-8');
$not_view = 1;
include_once 'lib.php';

$type = $_REQUEST['type'];

switch ($type){
    case 'add':
        $filename = upload_image_with_resize($_FILES['image'],500,375,$_SERVER['DOCUMENT_ROOT'] . '/upload/' , random_filename());
        $filename_p = upload_image($_FILES['image_p'],$_SERVER['DOCUMENT_ROOT'] . '/upload/' , random_filename());
        if(!file_exists($filename) && (filesize($filename) == FALSE || filesize($filename) == 0)){
           ?><script>alert('썸네일 이미지 업로드를 실패했습니다.\n확인후 다시 시도해주세요.'); location.back();</script><?php
            break;
        }
        if(!$filename_p || (!file_exists($filename_p) && (filesize($filename_p) == FALSE || filesize($filename_p) == 0))){
            ?><script>alert('상세 이미지 업로드를 실패했습니다.\n확인후 다시 시도해주세요.'); location.back();</script><?php
            break;
        }
        if(!($conn->addproduct($_REQUEST['name'],$_REQUEST['grow'],str_replace($_SERVER['DOCUMENT_ROOT'],'',$filename),str_replace($_SERVER['DOCUMENT_ROOT'],'',$filename_p),$_REQUEST['more_url'],$_REQUEST['shop_url'],$_REQUEST['info'],$_REQUEST['remarks']))){
            ?><script>alert('상품추가을 실패했습니다.\n확인후 다시 시도해주세요.'); location.back();</script><?php
            break;
        }
        ?><script>alert('추가되었습니다.'); location.href='../pages/product.php'</script><?php
        break;
    case 'edit':
        if($_FILES['image']['size'] !== 0 && $_FILES['image']['error'] == 0){
            $filename = upload_image_with_resize($_FILES['image'],500,375,$_SERVER['DOCUMENT_ROOT'] . '/upload/' , random_filename());
            if(!file_exists($filename) && (filesize($filename) == FALSE || filesize($filename) == 0)){
                ?><script>alert('썸네일 이미지 업로드를 실패했습니다.\n확인후 다시 시도해주세요.'); location.back();</script><?php
                break;
            }else{
                $filename = str_replace($_SERVER['DOCUMENT_ROOT'],'',$filename);
            }
        }else{
            $filename = $_REQUEST['image_url'];
        }

        if($_FILES['image_p']['size'] !== 0 && $_FILES['image_p']['error'] == 0){
            $filename_p = upload_image($_FILES['image_p'],$_SERVER['DOCUMENT_ROOT'] . '/upload/' , random_filename());
            if(!$filename_p || (!file_exists($filename_p) && (filesize($filename_p) == FALSE || filesize($filename_p) == 0))){
                ?><script>alert('상세 이미지 업로드를 실패했습니다.\n확인후 다시 시도해주세요.'); location.back();</script><?php
                break;
            }else{
                $filename_p = str_replace($_SERVER['DOCUMENT_ROOT'],'',$filename_p);
            }
        }else{
            $filename_p = $_REQUEST['image_url_p'];
        }

        if(!($conn->updateproduct($_REQUEST['id'],$_REQUEST['name'],$_REQUEST['grow'],str_replace($_SERVER['DOCUMENT_ROOT'],'',$filename),str_replace($_SERVER['DOCUMENT_ROOT'],'',$filename_p),$_REQUEST['more_url'],$_REQUEST['shop_url'],$_REQUEST['info'],$_REQUEST['remarks']))){
            ?><script>alert('상품수정을 실패했습니다.\n확인후 다시 시도해주세요.'); location.back();</script><?php
            break;
        }
        ?><script>alert('수정되었습니다.'); location.href='../pages/product.php'</script><?php
        break;
    case 'delete':
        if(!($conn->deleteproduct($_REQUEST['id']))){
            header("HTTP/1.0 500 DELETE FALSE");
        }
        break;
    case 'get':
        $data = $conn->getproduct_byid($_REQUEST['id']);
        if(!$data){
            header("HTTP/1.0 500 GET FALSE");
        }else{
            ?>
            <div class="form-group" style="display:none">
                <label for="recipient-name" class="col-form-label">Hidden Value</label>
                <input type="hidden" class="form-control" name="type" value="edit">
                <input type="hidden" class="form-control" name="id" value="<?=$data['uid']?>">
                <input type="hidden" class="form-control" name="image_url" value="<?=$data['image']?>">
                <input type="hidden" class="form-control" name="image_url_p" value="<?=$data['image_p']?>">
            </div>
            <div class="form-group">
                <label for="recipient-name" class="col-form-label">이름</label>
                <input type="text" class="form-control" name="name" placeholder="이름을 입력해주세요." value="<?=$data['name']?>" required>
            </div>
            <div class="form-group">
                <label for="recipient-name" class="col-form-label">수확날짜(월)</label>
                <input type="number" min="1" max="12" class="form-control" placeholder="숫자만 입력해주세요(1~12)" name="grow" value="<?=$data['grow']?>" required>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-6">
                        <label for="recipient-name" class="col-form-label">이미지(썸네일)</label>
                        <div class="input-group">
                        <span class="input-group-addon">
                            <img src="<?=$data['image']?>" style="width: 100%; height:auto"/>
                        </span>
                        </div>
                        <input type="file" class="form-control" placeholder="변경하실러면 이미지을 선택해주세요." name="image" accept="image/*">
                    </div>
                    <div class="col-lg-6">
                        <label for="recipient-name" class="col-form-label">이미지(상세)</label>
                        <div class="input-group">
                        <span class="input-group-addon">
                            <img src="<?=$data['image_p']?>" style="width: 100%; height:auto"/>
                        </span>
                        </div>
                        <input type="file" class="form-control" placeholder="변경하실러면 이미지을 선택해주세요." name="image_p" accept="image/*">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="recipient-name" class="col-form-label">상품구매URL</label>
                <input type="text" class="form-control" name="shop_url" placeholder="선택(기본값:#)" value="<?=$data['shop_url']?>">
            </div>
            <div class="form-group">
                <label for="message-text" class="col-form-label">상세정보</label>
                <textarea style="min-height: 300px; resize: vertical" class="form-control" name="info" placeholder="상세정보를 입력해주세요." required><?=$data['info']?></textarea>
            </div>
            <?php
        }
        break;
    case 'edit_user':
        if($_REQUEST['password'] != $_REQUEST['password_re']){
            ?><script>alert('비밀번호와 확인란에 비번이 다릅니다.\n확인후 다시 시도해주세요.'); location.back();</script><?php
            break;
        }

        if(!$conn->editeuser($_REQUEST['id'],$_REQUEST['email'],$_REQUEST['nickname'],$_REQUEST['password'])){
            ?><script>alert('계정정보수정 실패하였습니다.'); location.back();</script><?php
            break;
        }
        ?><script>alert('수정되었습니다.'); location.href='../pages/user.php'</script><?php

        break;
    case 'get_u':
        $data = $conn->getproduct_byid($_REQUEST['id']);
        if(!$data){
            header("HTTP/1.0 500 GET FALSE");
        }else{
            echo json_encode($data);
        }
        break;
    default:
        echo '잘못된 요청입니다.';
        break;
}


//함수처리
function random_filename(){
    return 'upload_' . strtotime(date("Y-m-d H:i:s"));
}


function upload_image_with_resize($image,$r_w,$r_h,$dir,$name){
    $tmp_image = $image['tmp_name'];
    $image_type = explode('/',$image['type'])[1];
    list($width,$height) = getimagesize($tmp_image);
    $revolution = $width / $height;

    if ($width > $height) {
        $width = ceil($width-($width*abs($revolution - $r_w / $r_h)));
    } else {
        $height = ceil($height-($height*abs($revolution - $r_w / $r_h)));
    }
    $newwidth = $r_w;
    $newheight = $r_h;


    $src = null;

    if(mb_strtolower($image_type) == 'jpg' || mb_strtolower($image_type) == 'jpeg'){
        $src = imagecreatefromjpeg($tmp_image);
    }else if (mb_strtolower($image_type) == 'png'){
        $src = imagecreatefrompng($tmp_image);
    }else if(mb_strtolower($image_type) == 'gif') {
        $src = imagecreatefromgif($tmp_image);
    }else if(mb_strtolower($image_type) == 'bmp'){
        $src = imagecreatefrombmp($tmp_image);
    }else {
        $src = imagecreatefromjpeg($tmp_image);
    }


    $dst = imagecreatetruecolor($newwidth, $newheight);
    imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);


    $dir = $dir . $name . '.' . $image_type;


    if(mb_strtolower($image_type) == 'jpg' || mb_strtolower($image_type) == 'jpeg'){
        imagejpeg($dst,$dir);
    }else if (mb_strtolower($image_type) == 'png'){
        imagepng($dst,$dir);
    }else if(mb_strtolower($image_type) == 'gif') {
        imagegif($dst,$dir);
    }else if(mb_strtolower($image_type) == 'bmp'){
        imagebmp($dst,$dir);
    }else {
        imagejpeg($dst,$dir);
    }
    return $dir;
}

function upload_image($image,$dir,$name){
    $image_type = explode('/',$image['type'])[1];
    $dir = $dir . $name . '.' . $image_type;
    if(move_uploaded_file($image['tmp_name'],$dir)){
        return $dir;
    }else{
        return false;
    }
}