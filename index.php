<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="HTML5 Template" />
<meta name="description" content="Sam Martin - Personal Vcard Resume HTML Template" />
<meta name="author" content="potenzaglobalsolutions.com" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>소나이팜</title>

<!-- Favicon -->
<link rel="shortcut icon" href="images/favicon.png" />

<!-- bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />

<!-- google font -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,300italic,400italic,600italic,700,700italic,800,800italic' rel='stylesheet' type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Raleway:400,100,100italic,200italic,200,300,300italic,400italic,500,500italic,600,600italic,700italic,900italic,900,800,700,800italic' rel='stylesheet' type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Noto+Sans' rel='stylesheet' type='text/css'>
<!-- themify icon -->
<link href="css/themify-icons.css" rel="stylesheet" type="text/css" />

<!-- font awesome  -->
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<!-- magnific popup -->
<link href="css/magnific-popup.css" rel="stylesheet" type="text/css" />

<!-- jquery ui -->
<link rel='stylesheet' href='css/jquery-ui.css'>
 
<!-- main style -->
<link href="css/style.css?v=1.2" rel="stylesheet" type="text/css" />

<!-- responsive -->
<link href="css/responsive.css" rel="stylesheet" type="text/css" />

<!-- custom -->
<link href="css/custom.css?v=3.3" rel="stylesheet" type="text/css" />
<link href="css/custom_mobile.css?v=1.3" rel="stylesheet" type="text/css" />

</head>

<body>

<!--=================================
  loading -->

 <div id="loading">
  <div id="loading-center">
    <div id="loading-center-absolute">
      <div class="object" id="object_one"></div>
      <div class="object" id="object_two"></div>
      <div class="object" id="object_three"></div>
      <div class="object" id="object_four"></div>
  </div>
 </div>
</div>

<!--=================================
  loading -->


<!--=================================
  header -->
  
<div class="menu-responsive"><a href="#"> <b>소나이팜</b></a> <a id="menu-icon" class="but" href="#"><span class="ti-menu"></span> </a></div>
<header id="left-header" class="header">
    <div id="menu-shadown" style="display:none; position:fixed; top:0; left: 0; width: 100%; height: 100%; background-color: rgba(0,0,0,0.5)" onclick="$('#menu-icon').click()"></div>
    <nav id="menu" class="navbar navbar-default" style="border-radius: 0px; ">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.html"> <img id="logo_img" src="images/logo.jpg" alt=""> </a>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul id="navbar" class="nav navbar-nav">
                <li class="active"><a href="#home" class="page-scroll">Home</a></li>
                <li><a href="#about" class="page-scroll">About</a></li>
                <li><a href="#blog" class="page-scroll">Product</a></li>
                <li><a href="#skill" class="page-scroll">Technology</a></li>
                <li><a href="#testimonials" class="page-scroll">Member</a></li>
                <li><a href="#contact" class="page-scroll">Contact</a></li>
            </ul>
        </div>
    </nav>
    <div class="menu-footer">
        <div class="social">
            <ul>
                <li><a href="https://www.instagram.com/sona2farm"><i class="fa fa-instagram" style="font-size: 20px;"></i></a></li>
                <li><a href="https://m.smartstore.naver.com/sona2farm"><i class="fa fa-shopping-cart" style="font-size: 20px;"></i></a></li>
            </ul>
        </div>
        <div class="copyright">
            <p>©소나이팜 <br/> all rights reserved</p>
        </div>
    </div>
</header>

<!--=================================
  header -->


<!--=================================
  intro -->

<section class="content-scroller">  
  <div id="home" class="intro-bg main-bg" style="background: url('images/logo.jpg');">
  <!--<div id="home" class="intro-bg" style="background-color: black">-->
   <div id="scroll-down" class="intro">
       <div class="intro-content">
           <div class="container-fluid">
               <div class="row" style="min-height: 558px;">
               </div>
           </div>
       </div>
   </div>
    <!--<a class="scroll-down page-scroll" title="Scroll Down" href="#about"><i></i></a>-->
</div>
  
<!--=================================
  intro --> 


<!--=================================
  about -->

<section id="about" class="about white-bg2 page-section" style="background-image: url('images/new/aboutBg_new.jpg');">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="contact-block">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="about-title">
                                농업이라는 신세계에 도전장을 던진<br/>청년 농부 <b>소나이팜</b>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="home-line"></div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="about-block">
                                <div class="about-info">안녕하세요,</div>
                                <div class="about-info">소나이팜 입니다.</div>
                                <div class="about-info">소나이는 제주도 방언으로 <b>사나이</b>를 뜻합니다.</div>
                                <div class="about-info">농업인 집안 부모님의 뒤를 이어 청년농업인으로써 농업에 종사하고 있습니다.</div>
                                <div class="about-info">GAP인증을 받아 안전한 먹거리를 위해 최선을 다하고 있습니다. </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--=================================
  about -->


    <!--=================================
 blog -->

    <section id="blog" class="blog white-bg page-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="section-title">
                        <div class="section-title-name">
                            <div class="product_header_title">Harvest Product</div>
                            <div class="product_header_sub">판매상품</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php
                include_once('./admin/lib/lib_u.php');
                $data = $conn->getproduct();
                foreach ($data as $dd) {

                    ?>
                    <div class="col-lg-6 col-md-6 col-sm-6 product_item">
                        <div class="blog-block">
                            <div class="blog-image">
                                <img class="img-responsive product_show"  data-product-no="<?=$dd['uid']?>" src="<?=$dd['image']?>" alt="">
                            </div>
                        </div>
                        <div class="blog-info product_show" data-product-no="<?=$dd['uid']?>" >
                            <div class="blog-content">
                                <a href="javascript: return false;"><?=$dd['name']?></a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>



<!--=================================
  My Skill -->

<section id="skill" class="my-skill white-bg page-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="section-title" style="margin-bottom: 20px;">
                    <div class="section-title-name" style="margin-top: -20px;">
                        <div class="general_title">Us Technology</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="ustech-info-title-top">마이크로플라즈마 기술을 이용한 오존수로 농작물의 신선도 개선</div>
                <div class="ustech-info-title-line" style="display:none"></div>
                <div class="ustech-info-title-bottom">“마이크로플라즈마 기술은 미국 University of Illinois에서 개발된 첨단의 대기압 저온 플라즈마 기술로,<br/>미국에서는 물과 공기의 정화에 효과적인 장치로 평가 받으며 활발한 연구개발과 상업화가 이루어지고 있습니다.”</div>
            </div>
        </div>
        <div class="row" style="margin-top:40px;">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="skill-content">
                    <div>
                        <div class="ustech-info-group">
                            <div class="ustech-info-title">오존은 강력한 살균력을 가집니다.</div>
                            <div class="ustech-info-info">오존은 친환경적인 모습과 달리 매우 강력한 산화력을 가지고 있어 염소 대비 약 3,000배(CT value)의 살균력을 가진 것으로 알려져 있습니다. 대신 염소의 살균력은 오존보다 약하지만 지속하는 성질을 가지고 있어 상호 보완하여 상용할 경우 큰 시너지를 낼 수 있습니다.</div>
                        </div>
                        <div class="ustech-info-group">
                            <div class="ustech-info-title">오존은 식품과 자연에 잔류하지 않고 산소로 분해되어 안전하고 환경친화적입니다.</div>
                            <div class="ustech-info-info">일반적인 살균과 소독에 사용되는 염소계 소독제와 달리 식품에 잔류하지 않습니다. 살균 및 소독 후 식품에 잔류하지 않기 때문에 다른 유해한 물질을 생성하지 않습니다. 오존은 산화작용 후 보다 덜 유독한 물질과 산소로 환원되어 환경친화적입니다.</div>
                        </div>
                        <div class="ustech-info-group">
                            <div class="ustech-info-title">농약, 유해가스, 중금속과 같은 유해물질을 안전하게 제거합니다.</div>
                            <div class="ustech-info-info">지하수 등에 유입된 농약 및 중금속 혹은 공해를 유발하는 각종 유기가스를 오존을 통해 분해 제거가 가능합니다. 또한 과일 채소 등의 식품 잔류 농약 또한 오존수 세척을 통하여 분해 제거가 가능하기 때문에 식품산업에 있어 오존가공을 통하여 안전성과 보존성을 함께 향상시켜줍니다.</div>
                        </div>
                        <div class="ustech-info-group">
                            <div class="ustech-info-title">농식품산업에서 오존 사용의 효과</div>
                            <div class="ustech-info-info">
                                1. 농산물 저장시 에틸렌을 제거하고 호흡율을 낮추어 신선도 유지에 도움을 줍니다.<br/>
                                2. 식품표면의 세정과 살균을 통해 식품의 안전성을 높이고 보존성을 향상시킵니다.<br/>
                                3. 포장 후 가열살균에 따른 영양소와 맛의 감소를 방지해 줍니다.<br/>
                                4. 환경친화적으로 물과 공기, 토양을 정화합니다.<br/>
                                5. 공기를 이용한 오존은 정화 후 산소로 자연적으로 전환되어 환경에 부담을 주지 않습니다.<br/>
                            </div>
                        </div>
                        <div class="ustech-info-company" onclick="location.href= 'http://www.ozonaid.com'">(주)오존에이드 : http://www.ozonaid.com</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="skill">
                    <div class="ustech-dart-image-group">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="ustech-dart-image-image" style="background-image: url('images/new/ustech/ustech1.png')"></div>
                                <div class="ustech-dart-image-name">살균</div>
                            </div>
                            <div class="col-xs-4">
                                <div class="ustech-dart-image-image" style="background-image: url('images/new/ustech/ustech2.png')"></div>
                                <div class="ustech-dart-image-name">분해</div>
                            </div>
                            <div class="col-xs-4">
                                <div class="ustech-dart-image-image" style="background-image: url('images/new/ustech/ustech3.png')"></div>
                                <div class="ustech-dart-image-name">자연순환</div>
                            </div>
                        </div>
                    </div>
                    <div class="ustech-dart-line"></div>
                    <div class="ustech-dart-bar-group">
                        <div class="ustech-dart-bar-title">저장성 개선</div>
                        <ul>
                            <li>오존수 살균 비부패율
                                <div class="bar_container">
                                  <span class="bar" data-bar='{ "color": "#07cb79" }'>
                                    <span class="pct">82.46%</span>
                                  </span>
                                </div>
                            </li>
                            <li>대조군 비부패율
                                <div class="bar_container">
                                 <span class="bar" data-bar='{ "color": "#07cb79", "delay": "600" }'>
                                   <span class="pct">42.31%</span>
                                 </span>
                                </div>
                            </li>
                        </ul>
                        <div class="skill-chart" style="margin-top:0px;">
                            <div class="skill-chart-expand clearfix">
                                <div class="expand expand-left">
                                    <p>0</p>
                                </div>
                                <div class="expand expand-left">
                                    <p>30</p>
                                </div>
                                <div class="expand expand-right">
                                    <p>60</p>
                                </div>
                                <div class="expand expand-right">
                                    <p>100</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="skill_ar" class="my-skill white-bg page-section" style="background:url('images/new/techBg.png')">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <div class="skill-content skill_ar-content">
                    <div class="row">
                        <div class="col-lg-12 col-md-12" style="min-width: 350px;">
                            <div class="artech_header">
                                <div class="artech_header_title">AR Market</div>
                                <div class="artech_header_line"></div>
                                <div class="artech_header_info">“가상현실(VR) 서비스는 강력한 컴퓨팅 기술과 IoT(사물인터넷) 기술의 발달로 실제와  가상 세계의 격차가 점차 좁아지면서 미디어, 게임, 스포츠 콘텐츠 이외에 업무에 디지털 전환에도 적용하는 추세입니다.”</div>
                            </div>
                            <div class="artech_download">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="artech_download_button" style="background-image: url('images/google.png')" onclick="location.href='https://play.google.com/store/apps/details?id=com.aiaracorp.sona2farm&hl=ko'"></div>
                                    </div>
                                    <div class="col-xs-6">
<!--                                        <div class="artech_download_button" style="background-image: url('images/apple.png')" onclick="location.href='https://itunes.apple.com/kr/app/google/id284815942'"></div>-->
                                        <div class="artech_download_button" style="background-image: url('images/apple.png')" onclick="alert('준비중입니다.')'"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="artech_info">
                                <div class="artech_info_title">AR마커 기능을 활용한 농작물 소개</div>
                                <div class="artech_info_info"> 소나이팜 어플을 다운로드받으시고 감귤박스에 인쇄된 이미지마커를 인식시키면 농작물의 효능과 설명을 확인하시고 쉽게 온라인쇼핑을 하실수 있습니다. </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--=================================
  My Skill --> 

<!--=================================
 calendar -->

<!--=================================
 calendar --> 

<div class="modal fade" id="product_modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class=" modal-close close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">상세보기</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6">
                       <img class="product_image" src="https://dummyimage.com/800/800/fff.png&text=dummy" style="width:100%; height: auto;"/>
                    </div>
                    <div class="col-lg-6">
                        <h2 class="product_name">이름</h2>
                        <h5 class="product_date">수확일 : 1월</h5>
                        <span class="product_info"></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
<!--                <button type="button" id="product_morebtn" class="btn btn-default" style="float:left" onclick="location.href= '/'">더보기</button>-->
                <button type="button" id="product_shopbtn" class="btn btn-default" style="float:left" onclick="location.href= '/'"><i class="fa fa-shopping-cart"></i></button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!--=================================
 calendar --> 
 

<!--=================================
 blog --> 

<section id="testimonials" class="testimonials white-bg page-section-pt">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="section-title">
                    <div class="section-title-name">
                        <!--<span>Farm member</span>-->
                        <!--<h2>Member </h2>-->
                        <p class="member_title">Farm Member</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 " style="padding-top: 60px;">
                <div class="testimonials-block">
                    <div class="testimonials-content" style="background-color: white; display:table">
                        <div class="avata_mask" style="background: url('images/testimonials/01.jpg'); background-size: cover; display: table-cell">
                        </div>
                        <div class="testimonials-comment" style="display:table-cell; vertical-align: middle;">
                            <span class="avata_name">김용환</span> <span class="avata_info">농장주</span> <br/>
                            <div class="avata_line"> </div>
                            <p class="avata_command">늘 좋은 품질의 귤을 생산하겠습니다</p>
                        </div>
                    </div>
                    <div class="testimonials-name">
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div style="height: 65px; text-align: center">
                    <img src="images/sonaifarm.png" alt="logo" style="list-height: 65px;"/>
                    <span style="font-size: 24px; line-height: 65px;">인스타그램</span>
                    <i class="fa fa-instagram" style="width: 32px; height: 65px; font-size: 32px; line-height: 65px;"></i>
                </div>
                <iframe src="http://lightwidget.com/widgets/6d84ed083bf65db98e181b966404bfa3.html" scrolling="yes" allowtransparency="true" class="lightwidget-widget" style="width:100%;border:0; margin-top: 15px; overflow:hidden;"></iframe>
            </div>
        </div>
    </div>
</section>

<!--=================================
 calendar --> 


<!--=================================
 contact --> 

<section id="contact" class="google-map white-bg page-section-pt">
 <div class="container-fluid">
     <div class="contact">
         <h2>Contact Us !</h2>
         <div class="address">
             <ul class="row">
                 <li class="col-xs-12 col-sm-12 col-md-9 col-lg-7"><span class="ti-location-pin"></span> <p>제주특별자치도 서귀포시 남원읍 한남리 1336-1 (검색창 제주그린팜 검색)</p> </li>
                 <li class="col-xs-12 col-sm-12 col-md-3 col-lg-5"><span class="ti-marker-alt"></span> <p>sona2farm@naver.com</p></li>
                 <li class="col-xs-12 col-sm-12 col-md-9 col-lg-7"><span class="ti-mobile"></span> <p>010-3264-2466</p></li>
                 <li class="col-xs-12 col-sm-12 col-md-3 col-lg-5"><span class="ti-printer"></span> <p>064-764-1810</p></li>
             </ul>
         </div>
         <div class="social">
             <ul>
                 <li><a href="https://www.instagram.com/sona2farm"><i class="fa fa-instagram" style="font-size: 30px;"></i></a></li>
                 <li><a href="https://m.smartstore.naver.com/sona2farm"><i class="fa fa-shopping-cart" style="font-size: 30px;"></i></a></li>
             </ul>
         </div>
     </div>
     <div id="google-map">
         <div id="map-canvas"></div>
     </div>
 </div>
</section>

<!--=================================
 contact --> 

</section>
 
<!--=================================
 javascript --> 
 
<!-- jquery -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- modernizr -->
<script type="text/javascript" src="js/modernizr.min.js"></script>

<!-- Plugins -->
<script type="text/javascript" src='js/plugins.js'></script>

<!-- map script -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDjVoaCW3PAn52C7WPpJ7NBBqU1_TUfnSI&callback=initMap"  type="text/javascript"></script>
<script type="text/javascript" src="js/map-scripts.js"></script>

<!-- datepicker -->
<script type="text/javascript" src="js/datepicker-script.js"></script>

<!-- custom -->
<script type="text/javascript" src="js/custom.js?v=1.4"></script>

<!-- LightWidget WIDGET -->
<script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script>
</body>
</html> 