
/*

Template: Sam Martin - Personal Vcard Resume HTML Template
Author: potenzaglobalsolutions.com
Version: 1.1 
Design and Developed by: potenzaglobalsolutions.com

NOTE:  

*/

/*================================================
[  Table of contents  ]
================================================
 
:: Preloader
:: Owl carousel
:: Menu left-header 
:: Menu scroll 
:: NiceScroll
:: PHP contact form 
:: Chart
:: Intro typer
:: Skill
:: Isotope
:: Popup gallery
:: Contect open
:: Placeholder

======================================
[ End table content ]
======================================*/

(function($){
  "use strict";
 
  $(window).load(function() {

  /*************************
         preloader
  *************************/  
   $("#load").fadeOut();
   $('#loading').delay(0).fadeOut('slow');

/*************************
        Owl carousel
*************************/
    $('.owl-carousel-1').owlCarousel({
       items:1,
       loop:true,
       autoplay:true,
       autoplayTimeout:2500,
       autoplayHoverPause:true, 
       smartSpeed:800,
       dots:true,
       nav:false
      }); 
  });

/*************************
      menu left-header
*************************/
$('.page-scroll').on('click',function(e){
    if ($(window).width() <= 992) {
        $('#menu-icon').click();
    }else{
        console.log('DeskTop!');
    }
});


$('#menu-icon').on( "click", function(e) {   
    e.preventDefault();
    e.stopPropagation();
    if(!$(this).hasClass('active')){
          $(this).addClass('active');
          $('#menu-shadown').show();
          $('#left-header').animate({'margin-left':230},300);
        } else {
          $(this).removeClass('active');
          $('#menu-shadown').hide();
          $('#left-header').animate({'margin-left':0},300);
    }
  return false;
});

$(window).resize(function() {
if ($(window).width() > 992) {

   $("#menu-icon").removeClass('active');
   $('#left-header').animate({'margin-left':0},300);
   }  
});

/*************************
        Menu scroll
*************************/
 $('#navbar,#scroll-down').on( "click", function(e) {
     if ( $(e.target).is('a.page-scroll') ) {
       if (location.pathname.replace(/^\//,'') == e.target.pathname.replace(/^\//,'') && location.hostname == e.target.hostname) {
          var target = $(e.target.hash);
            target = target.length ? target : $('[name=' + e.target.hash.slice(1) +']');
            if (target.length) {
                var gap = 0;              
                $('html,body').animate({
                  scrollTop: target.offset().top - gap
                }, 900);
                     
             }
          }
         return false;
      }
  });
  $('body').scrollspy({ 
        target: '.navbar-default',
        offset: 80
    })
   
/*************************
        NiceScroll
*************************/ 
  $("html").niceScroll({
    scrollspeed: false,
    mousescrollstep: 38,
    cursorwidth: 7,
    cursorborder: 0,
    cursorcolor: '#2f3742',
    autohidemode: true,
    zindex: 999999999,
    horizrailenabled: false,
    cursorborderradius: 0 
  }); 
  $(".navbar").niceScroll({
    scrollspeed: 150,
    mousescrollstep: 38,
    cursorwidth: 5,
    cursorborder: 0,
    cursorcolor: '#2f3742',
    autohidemode: true,
    zindex: 999999999,
    horizrailenabled: false,
    cursorborderradius: 0,
  });

/*************************
     php contact form 
*************************/
  $( "#contactform" ).submit(function( e ) {
    $("#ajaxloader").show();
    $("#contactform").hide();
    $.ajax({
      url:'php/contact-form.php',
      data:$(this).serialize(),
      type:'post',
      success:function(response){
        $("#ajaxloader").hide();
        $("#contactform").show();
        $("#contactform").find("input, textarea").val("");
        $("#formmessage").html(response).show().delay(2000).fadeOut('slow');
      }
    });
    e.preventDefault();
  });

/*************************
         chart
*************************/
 $('.language-skills').appear(function() {
            $('.chart').easyPieChart({
                easing: 'easeOutBounce',
                lineWidth: 8,
                size: 150,
                scaleColor: false,
                barColor: '#07cb79',
                trackColor: '#f7f7f7',
                animate: 7000,
                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });
          
           },
      {
     offset: 400 
 });

/*************************
       intro typer
*************************/
  var win = $(window),
      foo = $('#typer');
      foo.typer(['<h2>오존수를 이용한 차차세대 농장</h2>', '<h2>오존수를 이용한 차세대 농장</h2>' ]);
            win.resize(function(){
                var fontSize = Math.max(Math.min(win.width() / (1 * 10), parseFloat(Number.POSITIVE_INFINITY)), parseFloat(Number.NEGATIVE_INFINITY));
                foo.css({
                    fontSize: fontSize * .3 + 'px'
            });
  }).resize();

/*************************
          skill
*************************/ 
$('.skill').appear(function() {
     $( ".bar" ).each( function() {
        var $bar = $( this ),
       $pct = $bar.find( ".pct" ),
       data = $bar.data( "bar" );
      setTimeout( function() {
         $bar
         .css( "background-color", data.color )
         .prop( "title", data.width )
         .animate({
         "width": $pct.html()
       }, 3000, function() {
      $pct.css({
       "color": data.color,
       "opacity": 1
     });
     });
     }, data.delay || 0 );   
   });
 }, {
  offset: 400
});

/*************************
          isotope
*************************/
  $(window).on("load resize",function(e){ 
  var $container = $('.isotope'),
      colWidth = function () {
        var w = $container.width(), 
        columnNum = 1,
        columnWidth = 0;
     return columnWidth;
      },
     isotope = function () {
      $container.isotope({
        resizable: true,
        itemSelector: '.grid-item',
        masonry: {
          columnWidth: colWidth(),
          gutterWidth: 10
        }
      });
    };
  isotope(); 
  var $isotopefilters = $('.isotope-filters');
  // bind filter button click
  $isotopefilters.on( 'click', 'button', function() {
    var filterValue = $( this ).attr('data-filter');
    $container.isotope({ filter: filterValue });
  });
    // change active class on buttons
   $isotopefilters.each( function( i, buttonGroup ) {
      var $buttonGroup = $( buttonGroup );
      $buttonGroup.on( 'click', 'button', function() {
        $buttonGroup.find('.active').removeClass('active');
        $( this ).addClass('active');
      });
    }); 
   }); 
 
 /*************************
      Popup gallery
*************************/
$('.popup-portfolio').magnificPopup({
        delegate: 'a.portfolio-img',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            titleSrc: function(item) {
                return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
            }
       }
});
 
 /*************************
        contect open
*************************/
var menu       = $(".contact-content"),
    toggle     = $(".contact-toggle"),
    toggleIcon = $(".contact-toggle span");

function toggleThatNav() {
  if (menu.hasClass("show-contact")) {
    if (!Modernizr.csstransforms) {
      menu.removeClass("show-contact");
      toggle.removeClass("show-contact");
      menu.animate({
        right: "-=300"
      }, 500);
      toggle.animate({
        right: "-=300"
      }, 500);
    } else {
      menu.removeClass("show-contact");
      toggle.removeClass("show-contact");
    }
  } else {
    if (!Modernizr.csstransforms) {
      menu.addClass("show-contact");
      toggle.addClass("show-contact");
      menu.css("right", "0px");
      toggle.css("right", "330px");
    } else {
      menu.addClass("show-contact");
      toggle.addClass("show-contact");
    } 
  }
}
  function changeToggleClass() {
    toggleIcon.toggleClass("ti-close");
    toggleIcon.toggleClass("ti-comments");
  }
  toggle.on("click", function(e) {
    toggleThatNav();
    changeToggleClass();
    return false;
  });
    // Keyboard Esc event support
  $(document).keyup(function(e) {
    if (e.keyCode == 27) {
      if (menu.hasClass("show-contact")) {
        if (!Modernizr.csstransforms) {
          menu.removeClass("show-contact");
          toggle.removeClass("show-contact");
          menu.css("right", "-300px");
          toggle.css("right", "30px");
          changeToggleClass();
        } else {
          menu.removeClass("show-contact");
          toggle.removeClass("show-contact");
          changeToggleClass();
        }
      }
    } 
});

/*********************************
           placeholder
**********************************/
$('[placeholder]').focus(function() {
 var input = $(this);
 if (input.val() == input.attr('placeholder')) {
  input.val('');
  input.removeClass('placeholder');
 }
}).blur(function() {
 var input = $(this);
 if (input.val() == '' || input.val() == input.attr('placeholder')) {
  input.addClass('placeholder');
  input.val(input.attr('placeholder'));
 }
}).blur().parents('form').submit(function() {
 $(this).find('[placeholder]').each(function() {
  var input = $(this);
  if (input.val() == input.attr('placeholder')) {
   input.val('');
  }
 })
});


})(jQuery);





/**
 **/
var product_list = [{ /*dummy_object*/},
    {
        title : '황금향' ,
        sub_title : '11월 수확' ,
        more : '#11',
        shop_url : '#12',
        image: './images/new/product/p1.png',
        infomation: '-한라봉과 천혜향의 교배종, 껍질이 얇고 수분이 많다.\n-다량의 비타민 c 함유, 감기예방,피로회복에 탁월\n-신진대사를 활발하게 해주고 콜레스테롤을 낮춰서 동맹경화를 예방해줌'
    },
    {
        title : '조생감귤' ,
        sub_title : '11월 수확' ,
        more : '#21',
        shop_url : '#22',
        image: './images/new/product/p2.png',
        infomation: '-감귤은 비타민을 다량 함유하고있어 감기예방,피로회복에 효과적\n-과육에 붙어 있는 하얀심지는 식이섬유와 펙틴 성분이 풍부해 변비와 대장암 예방에도 효과적'
    },
    {
        title : '한라봉' ,
        sub_title : '2월 수확' ,
        more : '#31',
        shop_url : '#32',
        image: './images/new/product/p3.png',
        infomation: '-동의보감에 따르면 위장장애 천식 가래 식욕부진 등에 효과\n-항산화효과가 있는 카로티노이드 및 페놀성 물질은 노화 또는 성인병 발병 억제\n-항암효과가 있는 플라보노이드, 리모노이드를 함유하고 있어 발암과정의 단계 억제\n-거칠어진, 피로회복 기능'
    },
    {
        title : '탐나향' ,
        sub_title : '1월 수확' ,
        more : '#41',
        shop_url : '#42',
        image: './images/new/product/p4.png',
        infomation: '--일본에서 개발된 품종으로 맛이 진하고 광택의 빛이 나오는 특징\n-맛은 천혜향이 가진 진한 맛을 느낄 수 있음\n-피부미용과 노화 예방에 좋은 베타 크롭틴을 다른 품종에 비해 다량함유\n-제주에서도 이 품종을 재배하는 농가가 손에 꼽을 정도'
    }
];


$('.product_show').click(function() {
    var product_no = $(this).attr('data-product-no');
    $.ajax({
        url : 'connection.php',
        type: 'post',
        async: false,
        // dataType : 'text',
        data: {
            type: 'get_u',
            id: product_no
        },success : function(_data){
            console.log(_data);
            var data = JSON.parse(_data);
            $('.product_name').empty().append(data.name);
            $('.product_date').empty().append('수확일 : ' + data.grow + '월');
            if(data.image_p == null || data.image_p == ''){
                $('.product_image').attr('src',data.image);
            }else{
                $('.product_image').attr('src',data.image_p);
            }
            $('.product_info').empty().append(data.info.replace(/(?:\r\n|\r|\n)/g, '<br>'));
            $('#product_modal').modal('show');
            document.getElementById('product_shopbtn').onclick = function(){location.href = data.shop_url;};
        },error : function(e){
            console.log(e);
            alert('데이터 가져오는 도중 오류가 발생하였습니다.');
        }
    });
});


